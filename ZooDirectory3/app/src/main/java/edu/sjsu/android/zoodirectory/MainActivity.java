package edu.sjsu.android.zoodirectory;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
    // use this setting to
    // improve performance if you know that changes
    // in content do not change the layout size
    // of the RecyclerView
        recyclerView.setHasFixedSize(true);
    // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        List<String> input = Arrays.asList("Elephant", "Lion", "Tiger", "Zebra", "Monkey");
        List<Integer> images = Arrays.asList(R.drawable.elephant, R.drawable.lion, R.drawable.tiger, R.drawable.zebra, R.drawable.monkey);
        mAdapter = new MyAdapter(input, images);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.information:
                // User chose the "Settings" item, show the app settings UI...
                Intent intent = new Intent(this, ZooInfoActivity.class);
                startActivity(intent);
                return true;
            case R.id.uninstall:
                Uri packageUri = Uri.parse("package:edu.sjsu.android.zoodirectory");
                Intent intent2 = new Intent(Intent.ACTION_UNINSTALL_PACKAGE, packageUri);
                startActivity(intent2);
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

}