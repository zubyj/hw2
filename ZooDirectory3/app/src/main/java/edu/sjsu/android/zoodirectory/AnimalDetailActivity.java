package edu.sjsu.android.zoodirectory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;
import java.util.List;
import androidx.appcompat.app.AppCompatActivity;

public class AnimalDetailActivity extends AppCompatActivity {

    private List<Integer> images = Arrays.asList(R.drawable.elephant, R.drawable.lion, R.drawable.tiger, R.drawable.zebra, R.drawable.monkey);
    private List<String> names = Arrays.asList("Elephant", "Lion", "Tiger", "Zebra", "Monkey");
    private List<String> descriptions = Arrays.asList(
            "Elephants have large tusks and rough skin",
            "The lion is a ferocious beast and great hunter",
            "The tiger is expert in sneak attacks.",
            "Zebras have stripes to confuse their predators when escaping.",
            "Monkeys are an extremely intelligent species"
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animal_detail);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        ImageView bigImg = (ImageView) findViewById(R.id.icon2);
        int pos = MyAdapter.clickPos;
        bigImg.setImageResource(images.get(pos));
        TextView name = (TextView) findViewById(R.id.animal_name);
        name.setText(names.get(pos));
        TextView descrip = (TextView) findViewById(R.id.description);
        descrip.setText(descriptions.get(pos));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.information:
                // User chose the "Settings" item, show the app settings UI...
                Intent intent = new Intent(this, ZooInfoActivity.class);
                startActivity(intent);
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

}

